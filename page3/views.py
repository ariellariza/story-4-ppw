from django.shortcuts import render,redirect
from .forms import formulir
from .models import jadwal

def schedule(request):
	if(request.method=="POST"):
		tmp_form = formulir(request.POST)
		print(request.POST)
		if(tmp_form.is_valid()):
			tmp_models = jadwal()
			tmp_models.waktu = tmp_form.cleaned_data["waktu_kegiatan"]
			tmp_models.nama = tmp_form.cleaned_data["nama_kegiatan"]
			tmp_models.tempat = tmp_form.cleaned_data["tempat_kegiatan"]
			tmp_models.kategori = tmp_form.cleaned_data["kategori_kegiatan"]
			tmp_models.save()
		return redirect("/jadwal")
	else:
		tmp_form = formulir()
		tmp_models = jadwal.objects.all()
		tmp_dic = {
			"formulir" : tmp_form,
			"jadwal" : tmp_models
		}
		return render(request,"page3/page3.html", tmp_dic)

def hapus(request, pk):
	if(request.method=="POST"):
		tmp_form = formulir(request.POST)
		print(request.POST)
		if(tmp_form.is_valid()):
			tmp_models = jadwal()
			tmp_models.waktu = tmp_form.cleaned_data["waktu_kegiatan"]
			tmp_models.nama = tmp_form.cleaned_data["nama_kegiatan"]
			tmp_models.tempat = tmp_form.cleaned_data["tempat_kegiatan"]
			tmp_models.kategori = tmp_form.cleaned_data["kategori_kegiatan"]
			tmp_models.save()
		return redirect("/jadwal")
	else:
		jadwal.objects.filter(pk=pk).delete()
		tmp_form = formulir()
		tmp_models = jadwal.objects.all()
		tmp_dic = {
			"formulir" : tmp_form,
			"jadwal" : tmp_models
		}
		return render(request,"page3/page3.html", tmp_dic)