from django.db import models

class jadwal(models.Model):
	waktu = models.DateTimeField()
	nama = models.CharField(blank=False, max_length=50)
	tempat = models.CharField(blank=False, max_length=50)
	kategori = models.CharField(blank=False, max_length=50)