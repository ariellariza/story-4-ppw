from django import forms

class formulir(forms.Form):
	waktu_kegiatan = forms.DateTimeField(input_formats=['%Y-%m-%dT%H:%M'], widget=forms.DateTimeInput(attrs={
		"class" : "form-control",
		"placeholder" : "dd/mm/yyyy hh:mm",
		"type" : "datetime-local",
		"required" : True
	}))

	nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
		"class" : "form-control",
		"placeholder" : "Nama Kegiatan",
		"type" : "text",
		"required" : True
	}))

	tempat_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
		"class" : "form-control",
		"placeholder" : "Tempat Kegiatan",
		"type" : "text",
		"required" : True
	}))

	kategori_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
		"class" : "form-control",
		"placeholder" : "Kategori Kegiatan",
		"type" : "text",
		"required" : True
	}))